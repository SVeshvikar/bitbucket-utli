package com.morrisons.bitbucket.endpoint;

import com.morrisons.bitbucket.model.BitBucketResponse;

public abstract class BaseBitbucketEndpoint extends BaseBitbucketExtEndpointService<BitBucketResponse> {

	@Override
	protected Class<BitBucketResponse> getEntityClass() {
		return BitBucketResponse.class;
	}

	@Override
	protected RuntimeException getExceptionForErrorResponse(String message) {
		return null;
	}
	
}