package com.morrisons.bitbucket.endpoint;

import com.morrisons.bitbucket.config.BitbucketUtilConfig;
import com.morrisons.bitbucket.config.ExternalServiceConfig;
import com.morrisons.bitbucket.model.BitBucketResponse;

public class BitbucketExtEndpoint extends BaseBitbucketExtEndpointService<BitBucketResponse> {

	@Override
	protected ExternalServiceConfig getServiceConfig(BitbucketUtilConfig bitbucketUtilConfig) {
		return bitbucketUtilConfig.getBitbucketServiceConfig();
	}

	@Override
	protected Class<BitBucketResponse> getEntityClass() {
		return BitBucketResponse.class;
	}

	@Override
	protected RuntimeException getExceptionForErrorResponse(String message) {
		return null;
	}
}