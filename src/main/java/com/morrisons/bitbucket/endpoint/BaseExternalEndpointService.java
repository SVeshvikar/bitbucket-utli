package com.morrisons.bitbucket.endpoint;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.morrisons.bitbucket.client.ParameterMapping;
import com.morrisons.bitbucket.client.ParameterMappingType;
import com.morrisons.bitbucket.config.BitbucketUtilConfig;
import com.morrisons.bitbucket.config.ExternalServiceConfig;

public abstract class BaseExternalEndpointService<T>
{

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseExternalEndpointService.class);

	public T getEntity(List<ParameterMapping> parameterMappings, BitbucketUtilConfig bitbucketUtilConfig, String url)
	{

		Client client = getClient(bitbucketUtilConfig);

		ExternalServiceConfig serviceConfig = getServiceConfig(bitbucketUtilConfig);
		WebTarget webTarget = null;
		if (url != null)
		{
			webTarget = client.target(url);
		}
		else
		{
			webTarget = client.target(serviceConfig.getUri());
			webTarget = setParameters(webTarget, parameterMappings, serviceConfig.getApiKey());
		}

		LOGGER.info("web target {} ", webTarget.getUri().toString());
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

		String authorization = serviceConfig.getAuthorization();
		if (authorization != null && !authorization.trim().isEmpty())
		{
			invocationBuilder.header(HttpHeaders.AUTHORIZATION, authorization);
		}

		Response response = invocationBuilder.get();

		if (!isResponseValid(response))
		{

			response.close();
			client.close();

			String reasonPhrase = null;
			StatusType statusType = response.getStatusInfo();
			if (statusType != null)
			{
				reasonPhrase = statusType.getReasonPhrase();
			}

			String inputs = getInputs(parameterMappings);

			String message = "Response Status : " + response.getStatus() + ", Reason : " + reasonPhrase + ", Inputs : [" + inputs + "]";
			RuntimeException e = getExceptionForErrorResponse(message);
			LOGGER.error(message, e);
			throw e;
		}

		T entity = null;

		if (response.getStatus() == 200)
		{
			entity = response.readEntity(getEntityClass());
		}

		response.close();
		client.close();

		return entity;
	}

	protected boolean isResponseValid(Response response)
	{
		LOGGER.debug("response.getStatus() : " + response.getStatus());
		return response.getStatus() == 200;
	}

	protected abstract Client getClient(BitbucketUtilConfig bitbucketUtilConfig);

	protected abstract ExternalServiceConfig getServiceConfig(BitbucketUtilConfig bitbucketUtilConfig);

	protected abstract Class<T> getEntityClass();

	protected abstract RuntimeException getExceptionForErrorResponse(String message);

	protected WebTarget setParameters(WebTarget webTargetIP, List<ParameterMapping> parameterMappings, String apiKey)
	{

		WebTarget webTarget = webTargetIP;

		if (parameterMappings != null && !parameterMappings.isEmpty())
		{

			for (ParameterMapping parameterMapping : parameterMappings)
			{

				String paramName = parameterMapping.getName();
				Object paramValue = parameterMapping.getValue();
				ParameterMappingType paramType = parameterMapping.getType();

				LOGGER.debug("paramType : {}, paramName : {}, paramValue : {}", paramType, paramName, paramValue);

				if (paramType == ParameterMappingType.PATH)
				{
					webTarget = webTarget.resolveTemplate(paramName, paramValue);
				}
				else if (paramType == ParameterMappingType.QUERY)
				{
					webTarget = webTarget.queryParam(paramName, paramValue);
				}
			}
		}
		if (StringUtils.isNotBlank(apiKey))
		{
			webTarget = webTarget.queryParam("apikey", apiKey);
		}

		return webTarget;
	}

	protected String getInputs(List<ParameterMapping> parameterMappings)
	{

		StringBuilder sb = new StringBuilder();

		if (parameterMappings != null && !parameterMappings.isEmpty())
		{

			boolean isSubsequent = false;
			for (ParameterMapping parameterMapping : parameterMappings)
			{

				String paramName = parameterMapping.getName();
				Object paramValue = parameterMapping.getValue();

				if (isSubsequent)
				{
					sb.append(", ");
				}
				sb.append(paramName + " : " + paramValue);
				isSubsequent = true;
			}
		}
		return sb.toString();
	}
}