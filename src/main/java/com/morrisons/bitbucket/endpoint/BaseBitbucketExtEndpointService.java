package com.morrisons.bitbucket.endpoint;

import javax.ws.rs.client.Client;

import com.google.inject.Inject;
import com.morrisons.bitbucket.config.BitbucketUtilConfig;

public abstract class BaseBitbucketExtEndpointService<T> extends BaseExternalEndpointService<T> {

	/*@Inject
	protected BitbucketUtilConfig bitbucketUtilConfig;*/

	@Override
	protected Client getClient(BitbucketUtilConfig bitbucketUtilConfig) {
		return bitbucketUtilConfig.getClient(getServiceConfig(bitbucketUtilConfig));
	}

}