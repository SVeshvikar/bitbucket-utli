
package com.morrisons.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "commit",
    "type",
    "links",
    "path"
})
public class FileModel {

    @JsonProperty("commit")
    private Commit commit;
    @JsonProperty("type")
    private String type;
    @JsonProperty("links")
    private Links__ links;
    @JsonProperty("path")
    private String path;

    @JsonProperty("commit")
    public Commit getCommit() {
        return commit;
    }

    @JsonProperty("commit")
    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("links")
    public Links__ getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(Links__ links) {
        this.links = links;
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

}
