
package com.morrisons.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self"
})
public class Links__ {

    @JsonProperty("self")
    private Self__ self;

    @JsonProperty("self")
    public Self__ getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(Self__ self) {
        this.self = self;
    }

}
