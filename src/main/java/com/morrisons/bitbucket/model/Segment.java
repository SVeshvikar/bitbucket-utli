
package com.morrisons.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "text",
    "match"
})
public class Segment {

    @JsonProperty("text")
    private String text;
    @JsonProperty("match")
    private Boolean match;

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("match")
    public Boolean getMatch() {
        return match;
    }

    @JsonProperty("match")
    public void setMatch(Boolean match) {
        this.match = match;
    }

}
