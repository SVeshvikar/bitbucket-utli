
package com.morrisons.bitbucket.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "content_matches",
    "path_matches",
    "type",
    "file",
    "content_match_count"
})
public class Value {

    @JsonProperty("content_matches")
    private List<ContentMatch> contentMatches = null;
    @JsonProperty("path_matches")
    private List<Object> pathMatches = null;
    @JsonProperty("type")
    private String type;
    @JsonProperty("file")
    private FileModel file;
    @JsonProperty("content_match_count")
    private Integer contentMatchCount;

    @JsonProperty("content_matches")
    public List<ContentMatch> getContentMatches() {
        return contentMatches;
    }

    @JsonProperty("content_matches")
    public void setContentMatches(List<ContentMatch> contentMatches) {
        this.contentMatches = contentMatches;
    }

    @JsonProperty("path_matches")
    public List<Object> getPathMatches() {
        return pathMatches;
    }

    @JsonProperty("path_matches")
    public void setPathMatches(List<Object> pathMatches) {
        this.pathMatches = pathMatches;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("file")
    public FileModel getFile() {
        return file;
    }

    @JsonProperty("file")
    public void setFile(FileModel file) {
        this.file = file;
    }

    @JsonProperty("content_match_count")
    public Integer getContentMatchCount() {
        return contentMatchCount;
    }

    @JsonProperty("content_match_count")
    public void setContentMatchCount(Integer contentMatchCount) {
        this.contentMatchCount = contentMatchCount;
    }

}
