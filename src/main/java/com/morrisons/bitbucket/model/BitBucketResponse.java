
package com.morrisons.bitbucket.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "next",
    "values",
    "query_substituted",
    "pagelen",
    "size",
    "page",
    "previous"
})
public class BitBucketResponse {

    @JsonProperty("next")
    private String next;
    @JsonProperty("values")
    private List<Value> values = null;
    @JsonProperty("query_substituted")
    private Boolean querySubstituted;
    @JsonProperty("pagelen")
    private Integer pagelen;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("page")
    private Integer page;
    @JsonProperty("previous")
    private String previous;

    @JsonProperty("next")
    public String getNext() {
        return next;
    }

    @JsonProperty("next")
    public void setNext(String next) {
        this.next = next;
    }

    @JsonProperty("values")
    public List<Value> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<Value> values) {
        this.values = values;
    }

    @JsonProperty("query_substituted")
    public Boolean getQuerySubstituted() {
        return querySubstituted;
    }

    @JsonProperty("query_substituted")
    public void setQuerySubstituted(Boolean querySubstituted) {
        this.querySubstituted = querySubstituted;
    }

    @JsonProperty("pagelen")
    public Integer getPagelen() {
        return pagelen;
    }

    @JsonProperty("pagelen")
    public void setPagelen(Integer pagelen) {
        this.pagelen = pagelen;
    }

    @JsonProperty("size")
    public Integer getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(Integer size) {
        this.size = size;
    }

    @JsonProperty("page")
    public Integer getPage() {
        return page;
    }

    @JsonProperty("page")
    public void setPage(Integer page) {
        this.page = page;
    }

    @JsonProperty("previous")
    public String getPrevious() {
        return previous;
    }

    @JsonProperty("previous")
    public void setPrevious(String previous) {
        this.previous = previous;
    }

}
