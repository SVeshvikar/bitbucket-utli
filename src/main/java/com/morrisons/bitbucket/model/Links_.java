
package com.morrisons.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self",
    "html",
    "avatar"
})
public class Links_ {

    @JsonProperty("self")
    private Self_ self;
    @JsonProperty("html")
    private Html_ html;
    @JsonProperty("avatar")
    private Avatar avatar;

    @JsonProperty("self")
    public Self_ getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(Self_ self) {
        this.self = self;
    }

    @JsonProperty("html")
    public Html_ getHtml() {
        return html;
    }

    @JsonProperty("html")
    public void setHtml(Html_ html) {
        this.html = html;
    }

    @JsonProperty("avatar")
    public Avatar getAvatar() {
        return avatar;
    }

    @JsonProperty("avatar")
    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

}
