package com.morrisons.bitbucket.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.morrisons.bitbucket.config.BitbucketUtilConfig;
import com.morrisons.bitbucket.resource.BitbucketResource;

import de.thomaskrille.dropwizard_template_config.TemplateConfigBundle;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.ResourceInstaller;

public class BitBucketApp extends Application<BitbucketUtilConfig> {
	private static final Logger LOGGER = LoggerFactory.getLogger(BitBucketApp.class);

	@Override
	@SuppressWarnings("all")
	public void initialize(Bootstrap<BitbucketUtilConfig> bootstrap) {
		LOGGER.info("{}","--------------initialize------------------");

		GuiceBundle<BitbucketUtilConfig> guiceBundle = GuiceBundle.<BitbucketUtilConfig>builder()
				.installers(ResourceInstaller.class).extensions(BitbucketResource.class).build();
		bootstrap.addBundle(guiceBundle);
		bootstrap.addBundle(new TemplateConfigBundle());
	}

	@Override
	public void run(BitbucketUtilConfig c, Environment e) throws Exception {
		LOGGER.info("{}","------------Running----------------");
		e.jersey().register(new BitbucketResource());
	}

	public static void main(String[] args) throws Exception {
		new BitBucketApp().run(args);
	}

}
