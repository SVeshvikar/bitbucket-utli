package com.morrisons.bitbucket.external.bitbucketService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.morrisons.bitbucket.client.ParameterMapping;
import com.morrisons.bitbucket.client.ParameterMappingType;
import com.morrisons.bitbucket.config.BitbucketUtilConfig;
import com.morrisons.bitbucket.endpoint.BitbucketExtEndpoint;
import com.morrisons.bitbucket.fileUtil.FileUtility;
import com.morrisons.bitbucket.model.BitBucketResponse;

public class BitbucketService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(BitbucketService.class);

	private static final String SEPARATER = ",";

	public void getProjectHavingKey(String project, String searchKey, BitbucketUtilConfig bitbucketUtilConfig) throws Exception
	{
		LOGGER.info("{}", "START : BitbucketService :getProjectHavingKey");
		LOGGER.debug("{} :", searchKey);
		List<ParameterMapping> parameterMappings = new ArrayList<>();
		String formattedSearchKey = searchKey + " ext:yml ext:json";
		if (!"all".equalsIgnoreCase(project))
		{
			formattedSearchKey = "project:" + project + " " + searchKey + " ext:yml ext:json ";
		}
		parameterMappings.add(new ParameterMapping("search_query", formattedSearchKey, ParameterMappingType.QUERY));
		parameterMappings.add(new ParameterMapping("fields", "+values.file.commit.repository", ParameterMappingType.QUERY));
		File file = new File(bitbucketUtilConfig.getOutputFilePath() + project + ".csv");
		BitbucketExtEndpoint bitBucketExternalEndpoint = new BitbucketExtEndpoint();
		BitBucketResponse bitBucketResponse = bitBucketExternalEndpoint.getEntity(parameterMappings, bitbucketUtilConfig, null);
		List<String> records = new ArrayList<>();
		int size = bitBucketResponse.getSize();
		try (FileWriter fileWriter = new FileWriter(file))
		{

			if (file.exists())
			{
				file.delete();
				String header = "projectHref" + SEPARATER + "projectName" + SEPARATER + "fileName" + SEPARATER + "searchKey" + System.lineSeparator();
				records.add(header);
			}
			records = FileUtility.createCsvRecords(bitBucketResponse, searchKey);

			FileUtility.writeRecordsInFile(records, fileWriter);
			boolean hasNextPage = true;
			while (hasNextPage)
			{
				String nextPageUrl = bitBucketResponse.getNext();
				LOGGER.info("next page  ------- {}", nextPageUrl);
				if (nextPageUrl != null && !StringUtils.isBlank(nextPageUrl))
				{

					bitBucketResponse = bitBucketExternalEndpoint.getEntity(parameterMappings, bitbucketUtilConfig, nextPageUrl);
					records = FileUtility.createCsvRecords(bitBucketResponse, searchKey);
					FileWriter fw = new FileWriter(file, true);

					FileUtility.writeRecordsInFile(records, fw);

				}
				else
				{
					hasNextPage = false;
				}
			}
			LOGGER.info("{}", "END : BitbucketService :getProjectHavingKey");
		}
		catch (IOException e)
		{
			LOGGER.error("{} :", e);
			throw e;
		}
		LOGGER.info("Total Records ========== {} : ", size);

	}
}
