package com.morrisons.bitbucket.config;

import javax.validation.constraints.NotNull;
import javax.ws.rs.client.Client;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class BitbucketUtilConfig extends Configuration {

	@NotEmpty
	private String version;

	@JsonProperty("swagger")
	public SwaggerBundleConfiguration swaggerBundleConfiguration;

	@NotNull
	@JsonProperty("clientConfig")
	private ExternalClientConfig clientConfig;

	@NotNull
	@JsonProperty("getBitbucketServiceConfig")
	private ExternalServiceConfig bitbucketServiceConfig;

	@JsonProperty("isTesting")
	private boolean isTesting;

	@JsonProperty("outputFilePath")
	private String outputFilePath;

	public Client getClient(ExternalServiceConfig serviceConfig) {

		return getClient(serviceConfig.getApiKey());
	}

	public Client getClient(String apikey) {

		return clientConfig.getClient(apikey);
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ExternalClientConfig getClientConfig() {
		return clientConfig;
	}

	public void setClientConfig(ExternalClientConfig clientConfig) {
		this.clientConfig = clientConfig;
	}

	public ExternalServiceConfig getBitbucketServiceConfig() {
		return bitbucketServiceConfig;
	}

	public void setBitbucketServiceConfig(ExternalServiceConfig bitbucketServiceConfig) {
		this.bitbucketServiceConfig = bitbucketServiceConfig;
	}

	public boolean isTesting() {
		return isTesting;
	}

	public void setTesting(boolean isTesting) {
		this.isTesting = isTesting;
	}

	public SwaggerBundleConfiguration getSwaggerBundleConfiguration() {
		return swaggerBundleConfiguration;
	}

	public void setSwaggerBundleConfiguration(SwaggerBundleConfiguration swaggerBundleConfiguration) {
		this.swaggerBundleConfiguration = swaggerBundleConfiguration;
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

}
