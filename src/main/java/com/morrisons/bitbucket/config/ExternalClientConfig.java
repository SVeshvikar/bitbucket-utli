package com.morrisons.bitbucket.config;

import javax.ws.rs.client.Client;

import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.JerseyClientBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExternalClientConfig {

	@JsonProperty("proxyURI")
	private String proxyURI;

	@JsonProperty("proxyHost")
	private String proxyHost;

	@JsonProperty("proxyPort")
	private String proxyPort;

	@JsonProperty("proxyUser")
	private String proxyUser;

	@JsonProperty("proxyPass")
	private String proxyPass;

	public Client getClient(String apikey) {

		ClientConfig clientConfig = new ClientConfig();

		if (apikey != null && !apikey.isEmpty()) {
			String proxyURIFinal = null;
			if (proxyURI != null && !proxyURI.trim().isEmpty()) {
				proxyURIFinal = proxyURI;
			} else if (proxyHost != null && !proxyHost.trim().isEmpty() && proxyPort != null
					&& !proxyPort.trim().isEmpty()) {
				proxyURIFinal = "http://" + proxyHost + ":" + proxyPort;
			}

			if (proxyURIFinal != null) {
				clientConfig.connectorProvider(new ApacheConnectorProvider());
				clientConfig.property(ClientProperties.PROXY_URI, proxyURIFinal);
				clientConfig.property(ClientProperties.PROXY_USERNAME, proxyUser);
				clientConfig.property(ClientProperties.PROXY_PASSWORD, proxyPass);
				clientConfig.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
			}
		} else {
			String proxyURIFinal = null;

			if (proxyURI != null && !proxyURI.trim().isEmpty()) {
				proxyURIFinal = proxyURI;
			} else if (proxyHost != null && !proxyHost.trim().isEmpty() && proxyPort != null
					&& !proxyPort.trim().isEmpty()) {
				proxyURIFinal = "http://" + proxyHost + ":" + proxyPort;
			}

			if (proxyURIFinal != null) {
				clientConfig.connectorProvider(new ApacheConnectorProvider());
				clientConfig.property(ClientProperties.PROXY_URI, proxyURIFinal);
				clientConfig.property(ClientProperties.PROXY_USERNAME, proxyUser);
				clientConfig.property(ClientProperties.PROXY_PASSWORD, proxyPass);
				clientConfig.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
			}
		}
		return JerseyClientBuilder.newClient(clientConfig);
	}

	public void setProxyURI(String proxyURI) {
		this.proxyURI = proxyURI;
	}

}