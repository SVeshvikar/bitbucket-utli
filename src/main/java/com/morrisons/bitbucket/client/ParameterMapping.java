package com.morrisons.bitbucket.client;
public class ParameterMapping {

	private String name;

	private Object value;

	private ParameterMappingType type;

	public ParameterMapping(String name, Object value, ParameterMappingType type) {

		super();

		this.name = name;
		this.value = value;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public ParameterMappingType getType() {
		return type;
	}

	public void setType(ParameterMappingType type) {
		this.type = type;
	}
}
