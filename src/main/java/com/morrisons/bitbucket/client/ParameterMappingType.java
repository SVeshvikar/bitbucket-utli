package com.morrisons.bitbucket.client;
public enum ParameterMappingType {

	PATH, QUERY, HEADER
}