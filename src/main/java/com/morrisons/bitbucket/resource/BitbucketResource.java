package com.morrisons.bitbucket.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.common.net.HttpHeaders;
import com.google.inject.Inject;
import com.morrisons.bitbucket.config.BitbucketUtilConfig;
import com.morrisons.bitbucket.external.bitbucketService.BitbucketService;
import com.morrisons.bitbucket.model.BitBucketResponse;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/bitbucket/v1")
@Api(value = "BitbucketUtility", description = "Bitbucket Utility")
public class BitbucketResource
{

	@Inject
	private BitbucketUtilConfig bitbucketUtilConfig;
	private static final Logger LOGGER = LoggerFactory.getLogger(BitbucketResource.class);

	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Fetch Data from Bitbucket")

	@Path("projects/{project}/search/{key}")
	@ApiResponses(value = {
									@ApiResponse(code = 400, message = "Invalid specification"),
									@ApiResponse(code = 404, message = "Projects not found")})

	public Response getProjectsHavingKey(@ApiParam(value = "project ", required = true) @PathParam("project") String project, @ApiParam(value = "Search Key ", required = true) @PathParam("key") String searchKey) throws Exception
	{

		BitbucketService bitbucketEndpointService = new BitbucketService();
		try
		{

			bitbucketEndpointService.getProjectHavingKey(project, searchKey, bitbucketUtilConfig);
			return Response.ok().build();
		}
		catch (Exception e)
		{
			LOGGER.error("{} :", e);
			throw e;
		}
	}

}
