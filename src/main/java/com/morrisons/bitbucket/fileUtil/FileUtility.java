package com.morrisons.bitbucket.fileUtil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.morrisons.bitbucket.model.BitBucketResponse;
import com.morrisons.bitbucket.model.FileModel;
import com.morrisons.bitbucket.model.Value;

public class FileUtility {

	private static final String SEPARATER = ",";
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtility.class);

	public static List<String> createCsvRecords(BitBucketResponse bitBucketResponse, String searchKey
			) {
		List<String> recordList = new ArrayList<>();
		List<Value> values = bitBucketResponse.getValues();
		try {
			for (Value value : values) {
				FileModel fileModel = value.getFile();
				String projectHref = fileModel.getCommit().getRepository().getLinks().getHtml().getHref();
				String projectName = fileModel.getCommit().getRepository().getName();
				String fileName = fileModel.getPath();

				String record = projectHref + SEPARATER + projectName + SEPARATER + fileName + SEPARATER + searchKey
						+ System.lineSeparator();
				recordList.add(record);
				// BitBucketDetailsCsv bitBucketDetailsCsv = new
				// BitBucketDetailsCsv();
				// bitBucketDetailsCsv.setFileName(fileName);
				// bitBucketDetailsCsv.setFilterKey(searchKey);
				// bitBucketDetailsCsv.setLink(projectHref);
				// bitBucketDetailsCsv.setProjectName(projectName);
			}

		} catch (Exception e) {
			LOGGER.error("{} :" + e);
		}
		//LOGGER.info("records:   --------------");
		return recordList;
	}

	public static void writeRecordsInFile(List<String> records, FileWriter fileWriter) throws IOException {
		BufferedWriter outputWriter = new BufferedWriter(fileWriter);
		LOGGER.debug("record lenght : {} :", records.size());
		for (String record : records) {
			outputWriter.write(record);
			//LOGGER.debug("{} :", record);
		}
		outputWriter.flush();
		outputWriter.close();
	}
}
